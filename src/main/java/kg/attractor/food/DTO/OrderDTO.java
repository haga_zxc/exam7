package kg.attractor.food.DTO;

import kg.attractor.food.models.Order;
import lombok.*;

import java.time.LocalDateTime;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class OrderDTO {
    public static OrderDTO from(Order order) {
        return builder()
                .id(order.getId())
                .meal_name(order.getMeal().getName())
                .meal_price(order.getMeal().getPrice())
                .user_name(order.getUser().getName())
                .user_email(order.getUser().getEmail())
                .date(order.getDate())
                .build();
    }

    private String id;
    private String meal_name;
    private int meal_price;
    private String user_name;
    private String user_email;
    private LocalDateTime date;
}
