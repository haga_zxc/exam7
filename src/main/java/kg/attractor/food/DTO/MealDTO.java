package kg.attractor.food.DTO;

import kg.attractor.food.models.Meal;
import lombok.*;


@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class MealDTO {
    public static MealDTO from(Meal meal) {
        return builder()
                .id(meal.getId())
                .name(meal.getName())
                .type(meal.getType())
                .price(meal.getPrice())
                .cafe_name(meal.getCafe().getName())
                .build();
    }

    private String id;
    private String name;
    private String type;
    private int price;
    private String cafe_name;
}
