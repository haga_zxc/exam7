package kg.attractor.food.utils;

import kg.attractor.food.models.*;
import kg.attractor.food.repository.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Configuration
public class InitDataBase {
    private static final Random r = new Random();

    @Bean
    CommandLineRunner initDatabase(UserRepository userRepository, CafeRepository cafeRepository,
                                   MealRepository mealRepository, OrderRepository orderRepository) {
        return (args) -> {
            userRepository.deleteAll();
            cafeRepository.deleteAll();
            mealRepository.deleteAll();
            orderRepository.deleteAll();

            var demoUser = User.makeUser();
            demoUser.setEmail("123@gmail.com");             //Username for Spring Security for test the program
            demoUser.setName("Arstan");
            demoUser.setPassword("qwerty");                  //Password for Spring Security for test the program
            userRepository.save(demoUser);

            List<User> users = Stream.generate(User::makeUser)
                    .limit(10)
                    .collect(toList());
            userRepository.saveAll(users);

            List<Cafe> cafes = Stream.generate(Cafe::makeCafe)
                    .limit(3)
                    .collect(toList());
            cafeRepository.saveAll(cafes);

            List<Meal> meals = Stream.generate(() -> Meal.makeMeal((cafes.get(r.nextInt(cafes.size())))))
                    .limit(50)
                    .collect(toList());
            mealRepository.saveAll(meals);

            List<Order> orders = Stream.generate(() -> Order.makeOrder(users.get(r.nextInt(users.size())), meals.get(r.nextInt(meals.size()))))
                    .limit(10)
                    .collect(toList());
            orderRepository.saveAll(orders);
            System.out.println("done");
        };
    }
}
