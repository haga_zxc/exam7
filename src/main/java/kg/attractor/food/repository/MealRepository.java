package kg.attractor.food.repository;

import kg.attractor.food.models.Meal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;


public interface MealRepository extends PagingAndSortingRepository<Meal, String> {
    Page<Meal> findAllByCafeId(Pageable pageable, String id);

    Optional<Meal> findByName(String name);
}
