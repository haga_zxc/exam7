package kg.attractor.food.repository;

import kg.attractor.food.models.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface OrderRepository extends PagingAndSortingRepository<Order, String> {
    Page<Order> findAllByUser_Id(String id, Pageable pageable);
}
