package kg.attractor.food.repository;

import kg.attractor.food.models.Cafe;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface CafeRepository extends PagingAndSortingRepository<Cafe, String> {
    Optional<Cafe> findByName(String name);

}
