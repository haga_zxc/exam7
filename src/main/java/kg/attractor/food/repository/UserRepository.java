package kg.attractor.food.repository;

import kg.attractor.food.models.User;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface UserRepository extends PagingAndSortingRepository<User, String> {
    Optional<User> findByEmail(String email);
}
