package kg.attractor.food.controller;

import kg.attractor.food.DTO.UserDTO;
import kg.attractor.food.anotation.ApiPageable;
import kg.attractor.food.service.UserService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService us;

    public UserController(UserService us) {
        this.us = us;
    }

    @ApiPageable
    @GetMapping
    public Slice<UserDTO> findUsers(@ApiIgnore Pageable pageable) {
        return us.findUsers(pageable);
    }

    @PostMapping(path = "/register", consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO addNewUser(@RequestBody UserDTO userData) {
        return us.registerUser(userData);
    }

}
