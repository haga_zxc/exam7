package kg.attractor.food.controller;

import kg.attractor.food.DTO.CafeDTO;
import kg.attractor.food.anotation.ApiPageable;
import kg.attractor.food.service.CafeService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;


@RestController
@RequestMapping("/cafes")
public class CafeController {
    private final CafeService cs;

    public CafeController(CafeService cs) {
        this.cs = cs;
    }


    @ApiPageable
    @GetMapping
    public Slice<CafeDTO> findCafes(@ApiIgnore Pageable pageable) {
        return cs.findCafes(pageable);
    }


}
