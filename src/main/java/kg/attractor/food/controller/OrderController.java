package kg.attractor.food.controller;

import kg.attractor.food.DTO.OrderDTO;
import kg.attractor.food.anotation.ApiPageable;
import kg.attractor.food.service.OrderService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/orders")
public class OrderController {
    private final OrderService os;

    public OrderController(OrderService os) {
        this.os = os;
    }


    @ApiPageable
    @GetMapping
    public Slice<OrderDTO> findOrders(@ApiIgnore Pageable pageable) {
        return os.findOrders(pageable);
    }

    @GetMapping("/{cafe_name}/{meal_name}")
    public String makeOrder(@PathVariable("cafe_name") String cafe_name,
                            @PathVariable("meal_name") String meal_name,
                            Authentication authentication) {
        return os.makeOrder(cafe_name, meal_name, authentication);
    }

    @ApiPageable
    @GetMapping("/{email}")
    public Slice<OrderDTO> findByUserId(@ApiIgnore Pageable pageable,
                                        @PathVariable("email") String email) {
        return os.findByUserEmail(pageable, email);
    }
}
