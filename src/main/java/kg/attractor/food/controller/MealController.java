package kg.attractor.food.controller;

import kg.attractor.food.DTO.MealDTO;
import kg.attractor.food.anotation.ApiPageable;
import kg.attractor.food.service.MealService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/meals")
public class MealController {
    private final MealService ms;

    public MealController(MealService ms) {
        this.ms = ms;
    }

    @ApiPageable
    @GetMapping("/{cafe_name}")
    public Slice<MealDTO> findPosts(@ApiIgnore Pageable pageable,
                                    @PathVariable("cafe_name") String cafe_name) {
        return ms.findAllByCafeName(pageable, cafe_name);
    }
}
