package kg.attractor.food.service;

import kg.attractor.food.DTO.CafeDTO;
import kg.attractor.food.repository.CafeRepository;
import kg.attractor.food.repository.MealRepository;
import kg.attractor.food.repository.OrderRepository;
import kg.attractor.food.repository.UserRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

@Service
public class CafeService {
    private final CafeRepository cr;
    private final UserRepository ur;
    private final MealRepository mr;
    private final OrderRepository or;

    public CafeService(CafeRepository cr, UserRepository ur, MealRepository mr, OrderRepository or) {
        this.cr = cr;
        this.ur = ur;
        this.mr = mr;
        this.or = or;
    }

    public Slice<CafeDTO> findCafes(Pageable pageable) {
        var slice = cr.findAll(pageable);
        return slice.map(CafeDTO::from);
    }


}
