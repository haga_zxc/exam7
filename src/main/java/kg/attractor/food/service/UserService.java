package kg.attractor.food.service;

import kg.attractor.food.DTO.UserDTO;
import kg.attractor.food.models.User;
import kg.attractor.food.repository.UserRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private final UserRepository ur;

    public UserService(UserRepository ur) {
        this.ur = ur;
    }


    public Slice<UserDTO> findUsers(Pageable pageable) {
        var slice = ur.findAll(pageable);
        return slice.map(UserDTO::from);
    }

    public UserDTO registerUser(UserDTO userDTO) {
        var user = User.builder()
                .id(userDTO.getId())
                .name(userDTO.getName())
                .email(userDTO.getEmail())
                .password(userDTO.getPassword())
                .build();
        ur.save(user);
        return UserDTO.from(user);
    }
}
