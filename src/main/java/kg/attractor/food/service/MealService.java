package kg.attractor.food.service;

import kg.attractor.food.DTO.MealDTO;
import kg.attractor.food.repository.CafeRepository;
import kg.attractor.food.repository.MealRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

@Service
public class MealService {
    private final MealRepository mr;
    private final CafeRepository cr;

    public MealService(MealRepository mr, CafeRepository cr) {
        this.mr = mr;
        this.cr = cr;
    }

    public Slice<MealDTO> findAllByCafeName(Pageable pageable, String name) {
        var cafe = cr.findByName(name);
        String id = cafe.get().getId();
        var slice = mr.findAllByCafeId(pageable, id);
        return slice.map(MealDTO::from);
    }
}
