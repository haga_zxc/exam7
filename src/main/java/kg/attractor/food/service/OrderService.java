package kg.attractor.food.service;

import kg.attractor.food.DTO.OrderDTO;
import kg.attractor.food.models.Order;
import kg.attractor.food.models.User;
import kg.attractor.food.repository.CafeRepository;
import kg.attractor.food.repository.MealRepository;
import kg.attractor.food.repository.OrderRepository;
import kg.attractor.food.repository.UserRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
    private final CafeRepository cr;
    private final UserRepository ur;
    private final MealRepository mr;
    private final OrderRepository or;

    public OrderService(CafeRepository cr, UserRepository ur, MealRepository mr, OrderRepository or) {
        this.cr = cr;
        this.ur = ur;
        this.mr = mr;
        this.or = or;
    }

    public Slice<OrderDTO> findOrders(Pageable pageable) {
        var slice = or.findAll(pageable);
        return slice.map(OrderDTO::from);
    }

    public Slice<OrderDTO> findByUserEmail(Pageable pageable, String email) {
        var user = ur.findByEmail(email);
        String id = user.get().getId();
        var order = or.findAllByUser_Id(id, pageable);
        return order.map(OrderDTO::from);
    }

    public String makeOrder(String cafe_name, String meal_name, Authentication authentication) {

        var meal = mr.findByName(meal_name);
        var cafe = cr.findByName(cafe_name);
        var user = (User)authentication.getPrincipal();

        if (meal.isEmpty()) {
            return "We dont have this meal";
        } else if (cafe.isEmpty()) {
            return "There is't any cafe with this name";
        } else if (!cafe.get().getId().equals(meal.get().getCafe().getId())) {
            return "There is't any meal with this name in this Cafe";
        } else {
            var order = Order.makeOrder(user, meal.get());
            or.save(order);
            return "You have successfully ordered your meal. You can check it by path = /orders/'email'";
        }
    }
}
