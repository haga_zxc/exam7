package kg.attractor.food.models;

import kg.attractor.food.utils.GenerateData;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;


@Document
@Data
@Builder
public class Cafe {
    @Id
    @Builder.Default
    private String id = UUID.randomUUID().toString();
    private String name;
    private String description;

    public static Cafe makeCafe() {
        return builder()
                .name(GenerateData.randomPlace().getName())
                .description(GenerateData.randomPlace().getDescription())
                .build();
    }


}
