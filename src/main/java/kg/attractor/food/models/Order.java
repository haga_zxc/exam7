package kg.attractor.food.models;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Document
@Builder
public class Order {
    @Id
    @Builder.Default
    private String id = UUID.randomUUID().toString();
    @Builder.Default
    @DBRef
    private User user;
    @Builder.Default
    @DBRef
    private Meal meal;
    private LocalDateTime date;

    public static Order makeOrder(User user, Meal meal) {
        return builder()
                .user(user)
                .meal(meal)
                .date(LocalDateTime.now())
                .build();
    }
}
