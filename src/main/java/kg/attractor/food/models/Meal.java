package kg.attractor.food.models;

import kg.attractor.food.utils.GenerateData;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Random;
import java.util.UUID;

@Document
@Data
@Builder
public class Meal {
    @Id
    @Builder.Default
    private String id = UUID.randomUUID().toString();
    private String name;
    private String type;
    private int price;
    @Builder.Default
    @DBRef
    private Cafe cafe;

    public static Meal makeMeal(Cafe cafe) {
        Random r = new Random();
        return builder()
                .name(GenerateData.randomDish().getName())
                .type(GenerateData.randomDish().getType())
                .cafe(cafe)
                .price(r.nextInt(500) + 100)
                .build();

    }
}
